# A tuple is an immutable object.  Create one by using
# parenthesis.
example_tuple = (9, 8, 7, -9)
print "example_tuple is:", example_tuple

# Alternatively, create a tuple from an 'iterable'
# like a list.
example_tuple = tuple([9,8,7,-9])
print "example_tuple is:", example_tuple

# We can index them, just like strings
print "example_tuple[2] is:", example_tuple[2]

# Can also loop through tuple objects like so
for item in example_tuple:
    print item

# Use the built in len function to see how long the tuple is
print "Tuple length is:", len(example_tuple)

# Can also loop through the indexes
for index in range(len(example_tuple)):
    print "Index is:", index
    print "Value at that index is:", example_tuple[index]

# A useful built in function is enumerate which is similar to the above
# example
for index, item in enumerate(example_tuple):
    print "Index is:", index
    print "Value at that index is:", item

# However can't redefine a single element like for a list

# uncomment line below to see an error
#example_tuple[1] = 77 # Returns an error


# Useful to know about "tuple unpacking"

(a, b, c, d) = example_tuple
print "a is:", a
print "b is:", b
print "c is:", c
print "d is:", d

# Make sure that you always have the same number of
# variables when you unpack a tuple!

# Tuples are immutable. To change a tuple, we would need
# to first unpack it, change the values, then repack it:

# Redefine b
b = -10

# redefines a
a = 0

# Repack the tuple
example_tuple = (a, b, c, d)
print "example_tuple is now:", example_tuple
