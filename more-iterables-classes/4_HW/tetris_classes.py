# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 15:01:59 2016

@author: Mike
"""

from graphics import *

class Block(Rectangle):
    BLOCK_OUTLINE = 3.0
    BLOCK_SIZE = 30.0
    def __init__(self, position, color):
        self.x = position.x
        self.y = position.y        
        
        upeLef = Point(position.x*Block.BLOCK_SIZE + Block.BLOCK_OUTLINE,
                       position.y*Block.BLOCK_SIZE + Block.BLOCK_OUTLINE)
        lowRig = Point(upeLef.x + Block.BLOCK_SIZE,
                       upeLef.y + Block.BLOCK_SIZE)
                       
        Rectangle.__init__(self, upeLef, lowRig)

        self.setWidth(Block.BLOCK_OUTLINE)
        self.setFill(color)
        
class Shape:
    def __init__(self, coords, color):
        self.blocks = [Block(coord, color) for coord in coords]        
        
    def draw(self, win):
        for block in self.blocks:
            block.draw(win)
            
class I_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 2, center.y),
                  Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y)]
        Shape.__init__(self, coords, "blue")     

class J_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x, center.y), 
                  Point(center.x + 1, center.y),
                  Point(center.x + 1, center.y + 1)]
        Shape.__init__(self, coords, "orange")

class L_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y + 1),
                  Point(center.x - 1, center.y),
                  Point(center.x, center.y),
                  Point(center.x + 1, center.y)]
        Shape.__init__(self, coords, "cyan")

class O_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x - 1, center.y + 1),
                  Point(center.x, center.y),
                  Point(center.x, center.y + 1)]
        Shape.__init__(self, coords, "red")

class S_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y + 1),
                  Point(center.x , center.y),
                  Point(center.x , center.y + 1),
                  Point(center.x + 1, center.y)]
        Shape.__init__(self, coords, "green")

class T_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x , center.y),
                  Point(center.x + 1 , center.y),
                  Point(center.x, center.y + 1)]
        Shape.__init__(self, coords, "yellow")

class Z_shape(Shape):
    def __init__(self, center):
        coords = [Point(center.x - 1, center.y),
                  Point(center.x , center.y),
                  Point(center.x , center.y + 1),
                  Point(center.x + 1, center.y + 1)]
        Shape.__init__(self, coords, "magenta")

def main():
    win = GraphWin('Tetrominoes', 900, 150)
    tetrominoes = [I_shape, J_shape, L_shape, O_shape, S_shape,
                   T_shape, Z_shape]
    x = 3
    for tetromino in tetrominoes:
        shape = tetromino(Point(x,1))
        shape.draw(win)
        x += 4
    
    win.mainloop()

if __name__ == "__main__":
    main()